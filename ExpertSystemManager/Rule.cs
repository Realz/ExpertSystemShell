﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemManager
{
    [Serializable]
    public class Rule
    {
        public string Name { get; set; }
        public string Reason { get; set; }
        public BindingList<Fact> Premises;
        public BindingList<Fact> Conclusions;
        public bool isWorked;
        public string Description {
            get {
                string text = "ЕСЛИ " + Premises.First().ToList;
                for (int i = 1; i < Premises.Count; i++)
                    text += " и " + Premises[i].ToList;
                text += " ТО " + Conclusions.First().ToList;
                for (int i = 1; i < Conclusions.Count; i++)
                    text += " и " + Conclusions[i].ToList;
                return text;
            }}

        public Rule()
        {
            Conclusions = new BindingList<Fact>();
            Premises = new BindingList<Fact>();
        }

        public Rule(Rule r)
        {
            Conclusions = new BindingList<Fact>();
            foreach (Fact f in r.Conclusions)
            {
                Conclusions.Add(new Fact(f));
            }            
            Premises = new BindingList<Fact>();
            foreach(Fact f in r.Premises)
            {
                Premises.Add(new Fact(f));
            }
            Name = r.Name;
            Reason = r.Reason;
        }


        public void Work()
        {
            foreach (var conc in Conclusions)
                conc.SetResult();
            isWorked = true;
        }

        public bool CanWork()
        {
            bool canWork = true;
            foreach (var cond in Premises)
            {
                canWork = canWork && cond.Test();
            }
            return canWork;
        }

        public bool HasValues()
        {
            bool hasValues = true;
            foreach (var cond in Premises)
            {
                hasValues = hasValues && cond.CanWork();
            }
            return hasValues;
        }
    }
}
