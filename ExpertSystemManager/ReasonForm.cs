﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemManager
{
    public partial class ReasonForm : Form
    {
        public ReasonForm(Variable Goal, List<Rule> rs, List<Variable> vs)
        {
            InitializeComponent();
            lbGoal.Text = Goal.Name;
            foreach (Variable v in vs)
            {
                tbVariables.AppendText(v.ToList + '\n');
            }
            tv.Nodes.Add("Цель: " + Goal.Name);
            foreach (var rule in rs)
            {
                int i = tv.Nodes.Add(new TreeNode(rule.Name + ": " + rule.Description));
                tv.Nodes[i].Nodes.Add(new TreeNode(rule.Reason));
            }
        }

        private void btExpand_Click(object sender, EventArgs e)
        {
            tv.ExpandAll();
        }

        private void btCollapse_Click(object sender, EventArgs e)
        {
            tv.CollapseAll();
        }
    }
}
