﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemManager
{
    [Serializable]
    public class Fact
    {
        public Variable var;
        public Value value;

        public string ToList {
            get {return String.Format("{0} = {1}", var.Name, value.Name); }
        }

        public Fact()
        {
        }

        public Fact(Fact f)
        {
            value = f.value;
            var = f.var;
        }

        public bool Test()
        {
            return !var.IsBroken && var.Value == value;
        }

        public void SetResult()
        {
            var.Value = value;
        }

        public bool CanWork()
        {
            return var.Value != null;
        }
    }
}
