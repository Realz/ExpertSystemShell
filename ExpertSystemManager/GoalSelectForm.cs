﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemManager
{
    public partial class GoalSelectForm : Form
    {
        Shell shell;
        Variable goal;
        public GoalSelectForm(Variable goal)
        {
            InitializeComponent();
            shell = Shell.Instance;
            this.goal = goal;
            cbGoal.DisplayMember = "Name";
            cbGoal.DataSource = shell.Variables;
            if (goal != null)
            {
                cbGoal.SelectedItem = goal;
            }
            else
            {
                if (shell.Variables.Count > 0)
                    goal = shell.Variables.First();
                else
                {
                    btOK.Enabled = false;
                    btOK.BackColor = DefaultBackColor;
                }                    
            }
        }

        public Variable GetGoal()
        {
            return goal;
        }

        private void cbGoal_SelectedIndexChanged(object sender, EventArgs e)
        {
            goal = shell.Variables[cbGoal.SelectedIndex];
        }
    }
}
