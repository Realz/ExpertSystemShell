﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemManager
{
    public partial class AddEditDomainForm : Form
    {      
        Domain domain;
        HashSet<string> domainNames;
        bool validName, validCount, hasChanged = false, btOkPressed = false;

        public AddEditDomainForm(FormType formType, Domain domain, bool isUsed = false, bool isCopy = false)
        {
            InitializeComponent();
            this.domain = domain;
            Functions.ChangeForm(this, btOK, formType, "домена");
            lbValues.DisplayMember = "Name";
            lbValues.DataSource = domain.Values;
            Shell shell = Shell.Instance;
            domainNames = new HashSet<string>(shell.Domains.Select(i => i.Name.ToLower()).ToList());
            
            if (formType == FormType.Edit)
            {
                tbDomainName.Text = domain.Name;                
            }
            else
            {
                tbDomainName.Text = "Домен" + (shell.DomainCount + 1).ToString();
            }
            hasChanged = false;

            if (isCopy)
                tbDomainName.Text = domain.Name + "Copy";
            if (isUsed)
            {
                lbValues.ClearSelected();
                gbDomainValues.Enabled = false;
            }
            Functions.CheckButtons(btAdd, btChange, btDel, domain.Values, tbValueInput.Text, lbValues.SelectedIndex);
            validateDomainName();
            ValidateCount();
            validatebtOk();
        }

        public Domain GetDomain()
        {
            return domain;
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            domain.Values.Add(new Value(tbValueInput.Text));
            tbValueInput.Clear();
            lbValues.ClearSelected();
            tbValueInput.Select();
            ValidateCount();
            validatebtOk();
            hasChanged = true;
        }

        private void tbValueInput_TextChanged(object sender, EventArgs e)
        {
            Functions.CheckButtons(btAdd, btChange, btDel, domain.Values, tbValueInput.Text, lbValues.SelectedIndex);
        }

        private void lbValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbValues.SelectedIndex != -1)
                tbValueInput.Text = domain.Values[lbValues.SelectedIndex].Name;
        }

        private void btDel_Click(object sender, EventArgs e)
        {
            domain.Values.RemoveAt(lbValues.SelectedIndex);
            tbValueInput.Clear();
            ValidateCount();
            validatebtOk();
            hasChanged = true;
        }

        private void btChange_Click(object sender, EventArgs e)
        {
            domain.Values[lbValues.SelectedIndex].Name = tbValueInput.Text;
            Functions.CheckButtons(btAdd, btChange, btDel, domain.Values, tbValueInput.Text, lbValues.SelectedIndex);
            lbValues.DataSource = null;
            lbValues.DisplayMember = "Name";
            lbValues.DataSource = domain.Values;
            hasChanged = true;
        }

        private void tbDomainName_TextChanged(object sender, EventArgs e)
        {            
            validateDomainName();
            validatebtOk();
            hasChanged = true;
        }

        private void validateDomainName()
        {
            if (!String.IsNullOrWhiteSpace(tbDomainName.Text) && !domainNames.Contains(tbDomainName.Text.ToLower()))
            {
                gbDomainName.ForeColor = DefaultForeColor;
                gbDomainName.Text = "Имя домена";
                validName = true;
                domain.Name = tbDomainName.Text;
            }
            else
            {
                gbDomainName.ForeColor = Color.Red;
                gbDomainName.Text = "Имя домена (Недопустимое)";
                validName = false;
            }
        }

        private void AddEditDomainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (hasChanged && !btOkPressed)
            {
                switch(MessageBox.Show("Сохранить изменения?", "Отмена", MessageBoxButtons.YesNoCancel))
                {
                    case DialogResult.Yes:
                        if (btOK.Enabled)
                            DialogResult = DialogResult.OK;
                        else
                            e.Cancel = true;
                        break;
                    case DialogResult.No: return;
                    default:
                        e.Cancel = true;
                        break;
                      
                }
            }
            btOkPressed = false;
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            btOkPressed = true;
        }

        private void lbValues_MouseDown(object sender, MouseEventArgs e)
        {
            if (lbValues.SelectedIndex != -1)
            {
                tbValueInput.Text = domain.Values[lbValues.SelectedIndex].Name;
                lbValues.DoDragDrop(lbValues.SelectedIndex, DragDropEffects.Move);               
            }            
        }

        private void lbValues_DragDrop(object sender, DragEventArgs e)
        {
            int from = (int)e.Data.GetData(typeof(int));
            if (from == -1)
                return;
            Point point = lbValues.PointToClient(new Point(e.X, e.Y));
            int to = lbValues.IndexFromPoint(point);            
            if (to == -1) to = domain.Values.Count - 1;
            if (from != to)
            {
                Functions.Swap(domain.Values, from, to);
                lbValues.SelectedIndex = to;
            }
        }

        private void lbValues_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ValidateCount()
        {
            if (domain.Values.Count > 0)
            {
                gbDomainValues.ForeColor =  DefaultForeColor;
                gbDomainValues.Text = "Значения домена";
                validCount = true;
            }
            else
            {
                gbDomainValues.ForeColor = Color.Red;
                gbDomainValues.Text = "Значения домена (Отсутствуют)";
                validCount = false;
            }

        }

        private void validatebtOk()
        {
            if (validName && validCount)
            {
                btOK.BackColor = Color.YellowGreen;
                btOK.Enabled = true;
            }
            else
            {
                btOK.BackColor = DefaultBackColor;
                btOK.Enabled = false;
            }
        }
    }
}
