﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemManager
{
    public interface IMyEqual
    {
        bool Equal(string other);
    }
}
