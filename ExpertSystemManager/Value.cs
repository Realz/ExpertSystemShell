﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemManager
{
    [Serializable]
    public class Value : IMyEqual
    {
        private string name;
        public Value(string n)
        {
            name = n;
        }

        public string Name { get => name; set => name = value; }

        public bool Equal(string other)
        {
            return name.Equals(other);
        }

    }
}
