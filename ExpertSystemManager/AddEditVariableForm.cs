﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemManager
{
    public partial class AddEditVariableForm : Form
    {
        Variable currentVariable;
        private int i { get { return cbDomain.SelectedIndex; } }
        Shell shell;
        HashSet<string> variableNames;
        bool validName, hasChanged = false, btOkPressed = false, initialization = true;

        public AddEditVariableForm(FormType formType, Variable _variable, bool isUsed = false, bool isReqested = false)
        {
            InitializeComponent();
            Functions.ChangeForm(this, btOK, formType, "переменной");
            currentVariable = _variable;
            shell = Shell.Instance;
            variableNames = new HashSet<string>(shell.Variables.Select(i => i.Name.ToLower()).ToList());
            lbDomainValues.DisplayMember = "Name";
            cbDomain.DisplayMember = "Name";
            cbDomain.DataSource = shell.Domains;
            initialization = false;
            if (formType == FormType.Add)
            {
                currentVariable.VarType = VariableType.Derivable;
                tbQuestion.Enabled = false;
                tbVariableName.Text = "Переменная" + (shell.VariablesCount + 1).ToString();
                cbDomain.SelectedIndex = -1;
                cbDomain.SelectedItem = shell.Domains.First();
                if (isReqested)
                    rbZapros.Enabled = false;
            }
            else
            {
                switch (_variable.VarType)
                {
                    case VariableType.Request:
                        rbZapros.Checked = true;
                        tbQuestion.Enabled = true;
                        hasChanged = true;
                        break;
                    case VariableType.Derivable:
                        rbVivod.Checked = true;
                        tbQuestion.Enabled = false;
                        hasChanged = true;
                        break;
                    case VariableType.ReqDerivable:
                        rbVivodZapros.Checked = true;
                        tbQuestion.Enabled = true;
                        hasChanged = true;
                        break;
                    default:
                        break;
                }
                tbVariableName.Text = currentVariable.Name;
                tbQuestion.Text = currentVariable.Question;
                cbDomain.SelectedItem = currentVariable.Domain;
                if (i != -1)
                    lbDomainValues.DataSource = shell.Domains[i].Values;               
                variableNames.Remove(currentVariable.Name.ToLower());
            }
            hasChanged = false;
            if (isUsed)
            {
                //gbDomain.Enabled = false;
                cbDomain.Enabled = btAddDomain.Enabled = false;
            }
            validateDomain();
            validateVariableName();
        }

        public Variable GetVariable()
        {
            return currentVariable;
        }

        private void rbVivod_Click(object sender, EventArgs e)
        {
            currentVariable.VarType = VariableType.Derivable;
            tbQuestion.Enabled = false;
            hasChanged = true;
        }

        private void rbZapros_Click(object sender, EventArgs e)
        {
            currentVariable.VarType = VariableType.Request;
            tbQuestion.Enabled = true;
            hasChanged = true;
        }

        private void rbVivodZapros_CheckedChanged(object sender, EventArgs e)
        {
            currentVariable.VarType = VariableType.ReqDerivable;
            tbQuestion.Enabled = true;
            hasChanged = true;
        }

        private void AddEditVariableForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (hasChanged && !btOkPressed)
            {
                switch (MessageBox.Show("Сохранить изменения?", "Отмена", MessageBoxButtons.YesNoCancel))
                {
                    case DialogResult.Yes:
                        if (btOK.Enabled)
                        {
                            DialogResult = DialogResult.OK;
                            if (currentVariable.VarType != VariableType.Derivable)
                            {
                                currentVariable.Question = tbQuestion.Text;
                            }
                        }                            
                        else
                            e.Cancel = true;
                        break;
                    case DialogResult.No: return;
                    default:
                        e.Cancel = true;
                        break;

                }
            }
            btOkPressed = false;
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (currentVariable.VarType != VariableType.Derivable)
            {
                currentVariable.Question = tbQuestion.Text;
            }
            btOkPressed = true;
        }

        private void tbVariableName_TextChanged(object sender, EventArgs e)
        {
            validateVariableName();
            hasChanged = true;
        }

        private void cbDomain_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initialization)
            {
                if (i != -1)
                {
                    currentVariable.Domain = shell.Domains[i];
                    lbDomainValues.DataSource = shell.Domains[i].Values;
                    lbDomainValues.ClearSelected();
                }
                validateDomain();
                validatebtOk();
            }
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btAddDomain_Click(object sender, EventArgs e)
        {
            AddEditDomainForm addDomain = new AddEditDomainForm(FormType.Add, new Domain());
            if (addDomain.ShowDialog() == DialogResult.OK)
            {
                shell.Domains.Add(addDomain.GetDomain());
                cbDomain.SelectedIndex = shell.Domains.Count - 1;
                lbDomainValues.DataSource = shell.Domains[i].Values;
                lbDomainValues.ClearSelected();
                currentVariable.Domain = addDomain.GetDomain();
                validateDomain();
                validatebtOk();
            }
        }

        private void validateVariableName()
        {
            if (!String.IsNullOrWhiteSpace(tbVariableName.Text) && !variableNames.Contains(tbVariableName.Text.ToLower()))
            {
                gbVariableName.ForeColor = DefaultForeColor;
                gbVariableName.Text = "Имя";
                validName = true;
                currentVariable.Name = tbVariableName.Text;
            }
            else
            {
                gbVariableName.ForeColor = Color.Red;
                gbVariableName.Text = "Имя (Недопустимое)";
                validName = false;
            }
            validatebtOk();
        }

        private void lbDomainValues_MouseDown(object sender, MouseEventArgs e)
        {
            if (lbDomainValues.SelectedIndex != -1)
            {
                lbDomainValues.DoDragDrop(lbDomainValues.SelectedIndex, DragDropEffects.Move);
            }
        }

        private void lbDomainValues_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void lbDomainValues_DragDrop(object sender, DragEventArgs e)
        {
            int from = (int)e.Data.GetData(typeof(int));
            if (from == -1)
                return;
            Point point = lbDomainValues.PointToClient(new Point(e.X, e.Y));
            int to = lbDomainValues.IndexFromPoint(point);
            if (to == -1) to = shell.Domains[i].Values.Count - 1;
            if (from != to)
            {
                Functions.Swap(shell.Domains[i].Values, from, to);
                lbDomainValues.SelectedIndex = to;
            }
        }

        private void validateDomain()
        {
            if (i != -1)
            {
                gbDomain.ForeColor = DefaultForeColor;
                gbDomain.Text = "Домен";
            }
            else
            {
                gbDomain.ForeColor = Color.Red;
                gbDomain.Text = "Домен (Выберите домен)";
            }
        }

        private void validatebtOk()
        {
            if (validName && i != -1)
            {
                btOK.BackColor = Color.YellowGreen;
                btOK.Enabled = true;
            }
            else
            {
                btOK.BackColor = DefaultBackColor;
                btOK.Enabled = false;
            }
        }
    }
}
