﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemManager
{
    [Serializable]
    public class Shell
    {
        public string Name;
        public BindingList<Rule> Rules;
        public BindingList<Domain> Domains;
        public BindingList<Variable> Variables;
        public List<Fact> MeantFacts;
        public int VariablesCount;
        public int DomainCount;
        public int RulesCount;

        private static Shell instance;

        private Shell()
        {
            Name = "Безымянная экспертная систма";
            Rules = new BindingList<Rule>();
            Domains = new BindingList<Domain>();
            Variables = new BindingList<Variable>();
            MeantFacts = new List<Fact>();
            VariablesCount = DomainCount = RulesCount = 0;
        }

        public static Shell Instance
        {
            get
            {
                if (instance == null)
                    instance = new Shell();
                return instance;
            }
            set
            {
                instance = value;
            }
        }
    }
}
