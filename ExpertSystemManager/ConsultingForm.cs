﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ExpertSystemManager
{
    enum LogType { Question, Answer, Goal }
    public partial class ConsultingForm : Form
    {
        Stack<Variable> goals;
        Variable mainGoal;
        Semaphore mutGetAnswer;
        Value selectedValue;
        bool consultFinised;
        List<Rule> workedRules;
        List<Variable> hasValues;
        HashSet<Variable> BrokenVariables;
        List<Rule> allRules;
        Thread consultingThread;

        public ConsultingForm(Variable Goal)
        {
            InitializeComponent();
            lbAnswers.DisplayMember = "Name";
            consultFinised = false;
            mainGoal = Goal;
            Text += mainGoal.Name;
            goals = new Stack<Variable>();
            BrokenVariables = new HashSet<Variable>();
            goals.Push(Goal);
            allRules = new List<Rule>(Shell.Instance.Rules);
            allRules.ForEach(x => x.isWorked = false);
            foreach(var v in Shell.Instance.Variables)
            {
                v.Value = null;
                v.IsBroken = false;
            }
            workedRules = new List<Rule>();
            hasValues = new List<Variable>();
            mutGetAnswer = new Semaphore(0, 1);
            consultingThread = new Thread(Consult);
            consultingThread.Start();
        }

        public List<Rule> getFieredRules()
        {
            return workedRules;
        }

        public List<Variable> getFieredVariables()
        {
            return hasValues;
        }

        private void FindVariable(Variable currentGoal)
        {
            /* Описать что делать, если переменная выводимая, Выводимо-запрашиваемая*/
            if (currentGoal.Value != null)
            {
                goals.Pop();
                return;
            }                
            if (currentGoal.VarType != VariableType.Request)
            {
                Rule r = allRules.Find(x => currentGoal.usedByConclusion.Contains(x));
                if (r == null)
                {
                    r = allRules.Find(x => currentGoal.usedByPermises.Contains(x));
                    if (currentGoal.VarType == VariableType.Derivable)
                    {
                        if (r != null)
                        {
                            allRules.Remove(r);
                            //workedRules.Add(r);
                        }
                        else
                        {
                            currentGoal.IsBroken = true;
                            goals.Pop();
                        }
                        return;
                    } // по сути должен спросить
                }
                else // нашли правило, где наша переменная в заключениях
                {
                    if (r.HasValues()) // если правило может сработать, то работаем
                    {
                        if (r.CanWork())
                        {
                            r.Work();
                            workedRules.Add(r);
                            foreach (Fact f in r.Conclusions)
                            {
                                hasValues.Add(f.var);
                            }
                        }                            
                        allRules.Remove(r);                        
                        return;

                    }
                    else // иначе добавляем в стек неозначенные переменных переменные из условия
                    {
                        for (int i = r.Premises.Count - 1; i >= 0; i--)
                        {
                            if (r.Premises[i].var.Value == null)
                            {
                                goals.Push(r.Premises[i].var);
                            }
                        }
                        return;
                    }
                }
            }
            //if (currentGoal.VarType != VariableType.Derivable)
            //{
            RequestVariable(currentGoal);
            //}
        }

        private void RequestVariable(Variable variable)
        {
            variable.Value = null;
            if (String.IsNullOrEmpty(variable.Question))
                rtbQuestion.Invoke(new Action(() => rtbQuestion.Text = variable.Name + '?'));
            else
                rtbQuestion.Invoke(new Action(() => rtbQuestion.Text = variable.Question + '?'));
            lbAnswers.Invoke(new Action(() => lbAnswers.DataSource = variable.Domain.Values ));
            rtbLog.Invoke(new Action(() => Log(rtbQuestion.Text, LogType.Question)));
            //while(variable.Value == null)
            //{
                mutGetAnswer.WaitOne();
                variable.Value = selectedValue;
            //}            
            hasValues.Add(variable);
            goals.Pop();
        }

        private void Log(string text, LogType logType)
        {
            switch (logType)
            {
                case LogType.Question:
                    rtbLog.AppendText("Вопрос: " + text + "\n");
                    break;
                case LogType.Answer:
                    rtbLog.AppendText("Ответ >> " + text + "\n\n");
                    break;
                case LogType.Goal:
                    rtbLog.AppendText(text);
                    break;
                default:
                    rtbLog.Text += "ERROR";
                    break;
            }
            rtbLog.SelectionStart = rtbLog.Text.Length;
            rtbLog.ScrollToCaret();
        }

        private void Consult()
        {
            while (goals.Count > 0)
            {
                FindVariable(goals.Peek());
            }
            string answer;
            if (mainGoal.Value == null)
            {
                answer = "Не удалось определить " + mainGoal.Name + ". Обращайтейсь к другой экспертной системе";                
            }
            else
            {
                answer = String.Format("Результат консультации: {0} = {1}", mainGoal.Name, mainGoal.Value.Name);

            }
            rtbLog.Invoke(new Action(() => Log(answer, LogType.Goal)));
            lbAnswers.Invoke(new Action(() => lbAnswers.DataSource = null));
            rtbQuestion.Invoke(new Action(() => rtbQuestion.Clear()));
            MessageBox.Show(answer);            
            consultFinised = true;
            btNext.Invoke(new Action(() => btNext.Text = "Закрыть"));
        }

        private void btNext_Click(object sender, EventArgs e)
        {
            if (consultFinised)
            {
                Close();
            }
            else
            {
                selectedValue = (Value)lbAnswers.SelectedItem;
                Log(selectedValue.Name, LogType.Answer);
                mutGetAnswer.Release(1);
            }
        }

        private void ConsultingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (consultingThread.ThreadState != ThreadState.Stopped)
            {
                consultingThread.Abort();
            }
        }
    }
}
