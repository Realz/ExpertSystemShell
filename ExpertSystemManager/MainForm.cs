﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemManager
{
    public enum FormType { Add, Edit };
    public enum FactType { Permises, Conclusion }
    public partial class MainForm : Form
    {
        Shell shell;
        BinaryFormatter bf;
        string FileName;
        Variable mainGoal;
        bool isDragDrop;
        int DragDropIndex;
        List<Rule> workedRules;
        List<Variable> hasValue;
        bool ConsultHappend;
        int i { get { return dgvRules.SelectedCells.Count > 0 ? dgvRules.SelectedCells[0].RowIndex : -1; } }
        public MainForm()
        {
            InitializeComponent();
            InitializeShell();
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            var addForm = new AddEditRuleForm(FormType.Add, new Rule());
            if (addForm.ShowDialog() == DialogResult.OK)
            {
                Rule r = addForm.GetRule();
                shell.RulesCount++;
                if (i == -1)
                {
                    shell.Rules.Add(r);
                    dgvRules.Rows[shell.Rules.Count - 1].Selected = true;                    
                }
                else
                {
                    shell.Rules.Insert(i + 1, r);
                    dgvRules.Rows[i+1].Selected = true;
                }
                
                foreach(Fact f in r.Conclusions)
                {
                    f.var.usedByConclusion.Add(r);
                }
                foreach (Fact f in r.Premises)
                {
                    f.var.usedByPermises.Add(r);
                }
                dgvRules.FirstDisplayedScrollingRowIndex = i;

            }
        }

        private void btChange_Click(object sender, EventArgs e)
        {
            var addForm = new AddEditRuleForm(FormType.Edit, new Rule(shell.Rules[i]));
            if (addForm.ShowDialog() == DialogResult.OK)
            {
                Rule r = addForm.GetRule();
                Rule oldR = shell.Rules[i];
                foreach (Fact f in oldR.Conclusions)
                {
                    f.var.usedByConclusion.Remove(oldR);
                }
                foreach (Fact f in oldR.Premises)
                {
                    f.var.usedByPermises.Remove(oldR);
                }
                foreach (Fact f in r.Conclusions)
                {
                    f.var.usedByConclusion.Add(oldR);
                }
                foreach (Fact f in r.Premises)
                {
                    f.var.usedByPermises.Add(oldR);
                }

                oldR.Name = r.Name;
                oldR.Premises = r.Premises;
                oldR.Conclusions = r.Conclusions;
                oldR.Reason = r.Reason;
                int index = i;
                dgvRules.ClearSelection();
                dgvRules.Rows[index].Selected = true;
                //dgvRules.Refresh();
            }
        }

        private void btDel_Click(object sender, EventArgs e)
        {
            Rule oldRule = shell.Rules[i];
            foreach (Fact f in oldRule.Conclusions)
            {
                f.var.usedByConclusion.Remove(oldRule);
            }
            foreach (Fact f in oldRule.Premises)
            {
                f.var.usedByPermises.Remove(oldRule);
            }
            shell.Rules.RemoveAt(i);
        }

        private void доменыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var domainForm = new DomainForm();
            domainForm.ShowDialog();
            dgvRules.Refresh();
        }

        private void переменныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var variableForm = new VariableForm();
            variableForm.ShowDialog();
            dgvRules.Refresh();
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChooseFilePathToSave();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                Filter = "Expert System files (*.esf)|*.esf",
                RestoreDirectory = true
            };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                FileName = ofd.FileName;
                Text = "Оболочка баз знаний : " + Path.GetFileNameWithoutExtension(FileName);
                OpenFile();
                dgvRules.DataSource = shell.Rules;
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(FileName))
                ChooseFilePathToSave();
            else
                SaveFile();
        }

        private void ChooseFilePathToSave()
        {
            SaveFileDialog sfd = new SaveFileDialog
            {
                Filter = "Expert System files (*.esf)|*.esf",
                RestoreDirectory = true
            };
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                FileName = sfd.FileName;
                Text = "Оболочка баз знаний : " + Path.GetFileNameWithoutExtension(FileName);
                shell.Name = Path.GetFileNameWithoutExtension(FileName);
                SaveFile();
            }
        }

        private void SaveFile()
        {
            using (FileStream fs = new FileStream(FileName, FileMode.Create))
            {
                bf.Serialize(fs, shell);
                MessageBox.Show("Сохранено успешно!");
            }
        }

        private void OpenFile()
        {
            using (FileStream fs = new FileStream(FileName, FileMode.Open))
            {
                try
                {
                    Shell.Instance = (Shell)bf.Deserialize(fs);
                    shell = Shell.Instance;
                }
                catch
                {
                    MessageBox.Show("Ошибка открытия файла");
                }

            }
        }

        private void dgvRules_SelectionChanged(object sender, EventArgs e)
        {
            if (isDragDrop)
            {
                isDragDrop = false;
                dgvRules.Rows[DragDropIndex].Selected = true;
            }
            else
            {
                if (i != -1)
                {
                    lbPermises.DataSource = shell.Rules[i].Premises;
                    lbConclusions.DataSource = shell.Rules[i].Conclusions;
                }
                else
                {
                    lbPermises.DataSource = lbConclusions.DataSource = null;
                    lbPermises.DisplayMember = "ToList";
                    lbConclusions.DisplayMember = "ToList";
                }
                lbConclusions.ClearSelected();
                lbPermises.ClearSelected();
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void начатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selectForm = new GoalSelectForm(mainGoal);
            if (selectForm.ShowDialog() == DialogResult.OK)
            {
                mainGoal = selectForm.GetGoal();
                ConsultingForm consult = new ConsultingForm(mainGoal);
                consult.ShowDialog();
                ConsultHappend = true;
                workedRules = consult.getFieredRules();
                hasValue = consult.getFieredVariables();
            }

        }

        private void новыйToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            Shell.Instance = null;
            InitializeShell();
        }

        private void InitializeShell()
        {
            shell = Shell.Instance;
            bf = new BinaryFormatter();
            dgvRules.AutoGenerateColumns = false;
            dgvRules.DataSource = shell.Rules;
            lbPermises.DisplayMember = "ToList";
            lbConclusions.DisplayMember = "ToList";
            Text = "Оболочка баз заний : " + shell.Name; 
        }

        private void dgvRules_MouseDown(object sender, MouseEventArgs e)
        {
            int index = dgvRules.HitTest(e.X, e.Y).RowIndex;
            if (index != -1)
            {
                dgvRules.Rows[index].Selected = true;
                dgvRules.DoDragDrop(i, DragDropEffects.Move);
            }
        }

        private void dgvRules_DragDrop(object sender, DragEventArgs e)
        {
            int from = (int)e.Data.GetData(typeof(int));
            if (from == -1)
                return;
            Point point = dgvRules.PointToClient(new Point(e.X, e.Y));
            int to = dgvRules.HitTest(point.X, point.Y).RowIndex;
            if (to == -1) to = shell.Variables.Count - 1;
            if (from != to)
            {
                Functions.Swap(shell.Rules, from, to);
                dgvRules.Rows[to].Selected = true;
                DragDropIndex = to;
                isDragDrop = true;
            }
        }

        private void dgvRules_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void показатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ConsultHappend)
            {
                ReasonForm rf = new ReasonForm(mainGoal, workedRules, hasValue);
                rf.ShowDialog();
            }            
        }
    }
}
