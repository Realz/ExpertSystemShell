﻿namespace ExpertSystemManager
{
    partial class AddEditRuleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gbName = new System.Windows.Forms.GroupBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.gbPremisess = new System.Windows.Forms.GroupBox();
            this.lbPermises = new System.Windows.Forms.ListBox();
            this.btAddPermises = new System.Windows.Forms.Button();
            this.btEditPermises = new System.Windows.Forms.Button();
            this.btDelPermises = new System.Windows.Forms.Button();
            this.gbConclusions = new System.Windows.Forms.GroupBox();
            this.lbConclusions = new System.Windows.Forms.ListBox();
            this.btAddConclusion = new System.Windows.Forms.Button();
            this.btEditConclusion = new System.Windows.Forms.Button();
            this.btDelConclusion = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.btOK = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rtbReason = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbName.SuspendLayout();
            this.gbPremisess.SuspendLayout();
            this.gbConclusions.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.gbName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gbPremisess, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.gbConclusions, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btCancel, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.btOK, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(284, 504);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // gbName
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gbName, 2);
            this.gbName.Controls.Add(this.tbName);
            this.gbName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbName.Location = new System.Drawing.Point(3, 3);
            this.gbName.Name = "gbName";
            this.gbName.Size = new System.Drawing.Size(278, 44);
            this.gbName.TabIndex = 0;
            this.gbName.TabStop = false;
            this.gbName.Text = "Имя правила";
            // 
            // tbName
            // 
            this.tbName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbName.Location = new System.Drawing.Point(3, 16);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(272, 20);
            this.tbName.TabIndex = 0;
            this.tbName.TextChanged += new System.EventHandler(this.tbName_TextChanged);
            // 
            // gbPremisess
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gbPremisess, 2);
            this.gbPremisess.Controls.Add(this.lbPermises);
            this.gbPremisess.Controls.Add(this.btAddPermises);
            this.gbPremisess.Controls.Add(this.btEditPermises);
            this.gbPremisess.Controls.Add(this.btDelPermises);
            this.gbPremisess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbPremisess.Location = new System.Drawing.Point(3, 53);
            this.gbPremisess.Name = "gbPremisess";
            this.gbPremisess.Size = new System.Drawing.Size(278, 156);
            this.gbPremisess.TabIndex = 1;
            this.gbPremisess.TabStop = false;
            this.gbPremisess.Text = "Условия правила";
            // 
            // lbPermises
            // 
            this.lbPermises.AllowDrop = true;
            this.lbPermises.DisplayMember = "ToList";
            this.lbPermises.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbPermises.FormattingEnabled = true;
            this.lbPermises.Location = new System.Drawing.Point(3, 16);
            this.lbPermises.Name = "lbPermises";
            this.lbPermises.Size = new System.Drawing.Size(272, 68);
            this.lbPermises.TabIndex = 3;
            this.lbPermises.TabStop = false;
            this.lbPermises.SelectedIndexChanged += new System.EventHandler(this.lbPermises_SelectedIndexChanged);
            this.lbPermises.DragDrop += new System.Windows.Forms.DragEventHandler(this.lbPermises_DragDrop);
            this.lbPermises.DragOver += new System.Windows.Forms.DragEventHandler(this.lbPermises_DragOver);
            this.lbPermises.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbPermises_MouseDown);
            // 
            // btAddPermises
            // 
            this.btAddPermises.BackColor = System.Drawing.Color.YellowGreen;
            this.btAddPermises.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btAddPermises.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAddPermises.ForeColor = System.Drawing.SystemColors.WindowText;
            this.btAddPermises.Location = new System.Drawing.Point(3, 84);
            this.btAddPermises.Name = "btAddPermises";
            this.btAddPermises.Size = new System.Drawing.Size(272, 23);
            this.btAddPermises.TabIndex = 1;
            this.btAddPermises.Text = "Добавить";
            this.btAddPermises.UseVisualStyleBackColor = false;
            this.btAddPermises.Click += new System.EventHandler(this.btAddPermises_Click);
            // 
            // btEditPermises
            // 
            this.btEditPermises.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btEditPermises.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEditPermises.ForeColor = System.Drawing.SystemColors.WindowText;
            this.btEditPermises.Location = new System.Drawing.Point(3, 107);
            this.btEditPermises.Name = "btEditPermises";
            this.btEditPermises.Size = new System.Drawing.Size(272, 23);
            this.btEditPermises.TabIndex = 2;
            this.btEditPermises.Text = "Редактировать";
            this.btEditPermises.UseVisualStyleBackColor = true;
            this.btEditPermises.Click += new System.EventHandler(this.btEditPermises_Click);
            // 
            // btDelPermises
            // 
            this.btDelPermises.BackColor = System.Drawing.Color.Tomato;
            this.btDelPermises.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btDelPermises.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btDelPermises.ForeColor = System.Drawing.SystemColors.WindowText;
            this.btDelPermises.Location = new System.Drawing.Point(3, 130);
            this.btDelPermises.Name = "btDelPermises";
            this.btDelPermises.Size = new System.Drawing.Size(272, 23);
            this.btDelPermises.TabIndex = 3;
            this.btDelPermises.Text = "Удалить";
            this.btDelPermises.UseVisualStyleBackColor = false;
            this.btDelPermises.Click += new System.EventHandler(this.btDelPermises_Click);
            // 
            // gbConclusions
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gbConclusions, 2);
            this.gbConclusions.Controls.Add(this.lbConclusions);
            this.gbConclusions.Controls.Add(this.btAddConclusion);
            this.gbConclusions.Controls.Add(this.btEditConclusion);
            this.gbConclusions.Controls.Add(this.btDelConclusion);
            this.gbConclusions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbConclusions.Location = new System.Drawing.Point(3, 215);
            this.gbConclusions.Name = "gbConclusions";
            this.gbConclusions.Size = new System.Drawing.Size(278, 156);
            this.gbConclusions.TabIndex = 2;
            this.gbConclusions.TabStop = false;
            this.gbConclusions.Text = "Заключения правила";
            // 
            // lbConclusions
            // 
            this.lbConclusions.AllowDrop = true;
            this.lbConclusions.DisplayMember = "ToList";
            this.lbConclusions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbConclusions.FormattingEnabled = true;
            this.lbConclusions.Location = new System.Drawing.Point(3, 16);
            this.lbConclusions.Name = "lbConclusions";
            this.lbConclusions.Size = new System.Drawing.Size(272, 68);
            this.lbConclusions.TabIndex = 6;
            this.lbConclusions.TabStop = false;
            this.lbConclusions.SelectedIndexChanged += new System.EventHandler(this.lbConclusions_SelectedIndexChanged);
            this.lbConclusions.DragDrop += new System.Windows.Forms.DragEventHandler(this.lbConclusions_DragDrop);
            this.lbConclusions.DragOver += new System.Windows.Forms.DragEventHandler(this.lbConclusions_DragOver);
            this.lbConclusions.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbConclusions_MouseDown);
            // 
            // btAddConclusion
            // 
            this.btAddConclusion.BackColor = System.Drawing.Color.YellowGreen;
            this.btAddConclusion.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btAddConclusion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAddConclusion.ForeColor = System.Drawing.SystemColors.WindowText;
            this.btAddConclusion.Location = new System.Drawing.Point(3, 84);
            this.btAddConclusion.Name = "btAddConclusion";
            this.btAddConclusion.Size = new System.Drawing.Size(272, 23);
            this.btAddConclusion.TabIndex = 4;
            this.btAddConclusion.Text = "Добавить";
            this.btAddConclusion.UseVisualStyleBackColor = false;
            this.btAddConclusion.Click += new System.EventHandler(this.btAddConclusion_Click);
            // 
            // btEditConclusion
            // 
            this.btEditConclusion.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btEditConclusion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEditConclusion.ForeColor = System.Drawing.SystemColors.WindowText;
            this.btEditConclusion.Location = new System.Drawing.Point(3, 107);
            this.btEditConclusion.Name = "btEditConclusion";
            this.btEditConclusion.Size = new System.Drawing.Size(272, 23);
            this.btEditConclusion.TabIndex = 5;
            this.btEditConclusion.Text = "Редактировать";
            this.btEditConclusion.UseVisualStyleBackColor = true;
            this.btEditConclusion.Click += new System.EventHandler(this.btEditConclusion_Click);
            // 
            // btDelConclusion
            // 
            this.btDelConclusion.BackColor = System.Drawing.Color.Tomato;
            this.btDelConclusion.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btDelConclusion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btDelConclusion.ForeColor = System.Drawing.SystemColors.WindowText;
            this.btDelConclusion.Location = new System.Drawing.Point(3, 130);
            this.btDelConclusion.Name = "btDelConclusion";
            this.btDelConclusion.Size = new System.Drawing.Size(272, 23);
            this.btDelConclusion.TabIndex = 6;
            this.btDelConclusion.Text = "Удалить";
            this.btDelConclusion.UseVisualStyleBackColor = false;
            this.btDelConclusion.Click += new System.EventHandler(this.btDelConclusion_Click);
            // 
            // btCancel
            // 
            this.btCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCancel.Location = new System.Drawing.Point(145, 477);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(136, 24);
            this.btCancel.TabIndex = 9;
            this.btCancel.Text = "Отмена";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // btOK
            // 
            this.btOK.BackColor = System.Drawing.Color.YellowGreen;
            this.btOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btOK.Location = new System.Drawing.Point(3, 477);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(136, 24);
            this.btOK.TabIndex = 8;
            this.btOK.Text = "btOK";
            this.btOK.UseVisualStyleBackColor = false;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // groupBox1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.groupBox1, 2);
            this.groupBox1.Controls.Add(this.rtbReason);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 377);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(278, 94);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Причина";
            // 
            // rtbReason
            // 
            this.rtbReason.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbReason.Location = new System.Drawing.Point(3, 16);
            this.rtbReason.Name = "rtbReason";
            this.rtbReason.Size = new System.Drawing.Size(272, 75);
            this.rtbReason.TabIndex = 7;
            this.rtbReason.Text = "";
            this.rtbReason.TextChanged += new System.EventHandler(this.rtbReason_TextChanged);
            // 
            // AddEditRuleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 504);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(300, 400);
            this.Name = "AddEditRuleForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddEditForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddEditRuleForm_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbName.ResumeLayout(false);
            this.gbName.PerformLayout();
            this.gbPremisess.ResumeLayout(false);
            this.gbConclusions.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gbName;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.GroupBox gbPremisess;
        private System.Windows.Forms.GroupBox gbConclusions;
        private System.Windows.Forms.Button btAddPermises;
        private System.Windows.Forms.Button btEditPermises;
        private System.Windows.Forms.Button btDelPermises;
        private System.Windows.Forms.Button btAddConclusion;
        private System.Windows.Forms.Button btEditConclusion;
        private System.Windows.Forms.Button btDelConclusion;
        private System.Windows.Forms.ListBox lbPermises;
        private System.Windows.Forms.ListBox lbConclusions;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox rtbReason;
    }
}