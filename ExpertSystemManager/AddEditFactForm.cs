﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemManager
{
    public partial class AddEditFactForm : Form
    {
        FormType formType;
        FactType factType;
        Fact currentFact;
        Shell shell;
        BindingList<Variable> variablesAllowed;
        bool Initializing = true;
        private int vari { get { return cbVariable.SelectedIndex; } }
        private int vali { get { return cbDomainValue.SelectedIndex; } }

        public AddEditFactForm(FormType fTpe, FactType factType, Fact fact)
        {
            InitializeComponent();
            formType = fTpe;
            this.factType = factType;
            currentFact = fact;
            shell = Shell.Instance;
            cbVariable.DisplayMember = "Name";
            cbDomainValue.DisplayMember = "Name";
            if (factType == FactType.Conclusion)
                variablesAllowed = new BindingList<Variable>(shell.Variables.Where(x => x.VarType != VariableType.Request).ToList());
            else
                variablesAllowed = shell.Variables;

            cbVariable.DataSource = variablesAllowed;



            if (fTpe == FormType.Edit)
            {
                cbVariable.SelectedItem = currentFact.var;
                cbDomainValue.DataSource = currentFact.var.Domain.Values;
                cbDomainValue.SelectedItem = currentFact.value;
            }
            else
            {
                currentFact.var = variablesAllowed.First();
                cbDomainValue.DataSource = currentFact.var.Domain.Values;
                currentFact.value = currentFact.var.Domain.Values.First();
            }
            Initializing = false;
                
            Functions.ChangeForm(this, btOK, formType, factType == FactType.Permises ? "условия" : "заключения");
            validatebtOK();
        }

        public Fact GetFact()
        {
            return currentFact;
        }

        private void cbVariable_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!Initializing)
            {
                if (vari != -1)
                {
                    cbDomainValue.DataSource = variablesAllowed[vari].Domain.Values;
                    currentFact.var = variablesAllowed[vari];
                    if (vali != -1)
                        currentFact.value = currentFact.var.Domain.Values[vali];
                }
                validatebtOK();
            }
        }

        private void cbDomainValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!Initializing)
            {
                if (vali != -1 && currentFact.var != null)
                {
                    currentFact.value = currentFact.var.Domain.Values[vali];
                }
            }
        }

        private void validatebtOK()
        {
            btOK.Enabled = vali != -1 && vari != -1;
        }

        private void btAddVariable_Click(object sender, EventArgs e)
        {
            AddEditVariableForm addVariableForm = new AddEditVariableForm(FormType.Add, new Variable(), false, factType == FactType.Conclusion);
            if (addVariableForm.ShowDialog() == DialogResult.OK)
            {
                
                shell.Variables.Add(addVariableForm.GetVariable());
                if (variablesAllowed != shell.Variables)
                    variablesAllowed.Add(addVariableForm.GetVariable());
                cbVariable.SelectedItem = addVariableForm.GetVariable();
                addVariableForm.GetVariable().Domain.usedBy.Add(addVariableForm.GetVariable());
                shell.VariablesCount++;
            }
        }
    }
}
