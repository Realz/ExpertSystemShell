﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemManager
{
    public partial class DomainForm : Form
    {
        Shell shell;
        private int i { get { return lbDomains.SelectedIndex; } }
        public DomainForm()
        {
            InitializeComponent();
            shell = Shell.Instance;
            lbDomains.DisplayMember = "Name";
            lbValues.DisplayMember = "Name";
            lbDomains.DataSource = shell.Domains;
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            var addForm = new AddEditDomainForm(FormType.Add, new Domain());
            if (addForm.ShowDialog() == DialogResult.OK)
            {
                shell.Domains.Add(addForm.GetDomain());
                shell.DomainCount++;
            }
            validateButtons();
        }

        private void btChange_Click(object sender, EventArgs e)
        {
            bool addCopy = false;
            lbDomains.BeginUpdate();
            Domain oldDomain = shell.Domains[i];

            AddEditDomainForm editForm;
            if (oldDomain.isUsed)
            {
                switch(MessageBox.Show("Домен используется, создать копию?", "Внимание!", MessageBoxButtons.YesNo))
                {
                    case DialogResult.Yes:
                        editForm = new AddEditDomainForm(FormType.Add, new Domain(oldDomain), false, true);
                        addCopy = true;
                        break;
                    case DialogResult.No:
                        shell.Domains.RemoveAt(i);
                        editForm = new AddEditDomainForm(FormType.Edit, new Domain(oldDomain), true);
                        break;
                    default:
                        return;
                }                    
            }
            else
            {
                shell.Domains.RemoveAt(i);
                editForm = new AddEditDomainForm(FormType.Edit, new Domain(oldDomain));
            }            
            if (editForm.ShowDialog() == DialogResult.OK)
            {
                if (addCopy)
                {
                    if (i == -1)
                        shell.Domains.Add(editForm.GetDomain());
                    else
                        shell.Domains.Insert(i+1, editForm.GetDomain());
                }
                else
                {
                    oldDomain.Name = editForm.GetDomain().Name;
                    oldDomain.Values = editForm.GetDomain().Values;
                }                
            }
            if (!addCopy)
            {
                if (i == -1)
                    shell.Domains.Add(oldDomain);
                else
                    shell.Domains.Insert(i, oldDomain);
            }

            validateButtons();
            lbDomains.EndUpdate();
        }

        private void lbDomains_SelectedIndexChanged(object sender, EventArgs e)
        {
            validateButtons();  
        }

        private void validateButtons()
        {
            btDel.Enabled = btChange.Enabled = i != -1;
            if(i != -1)
            {
                lbValues.DataSource = shell.Domains[i].Values;
                lbValues.ClearSelected();
            }
        }

        private void btDel_Click(object sender, EventArgs e)
        {
            if (shell.Domains[i].isUsed)
                MessageBox.Show("Домен используется");
            else
            {
                shell.Domains.RemoveAt(i);
                if (i == -1)
                {
                    lbValues.DataSource = null;
                    lbValues.DisplayMember = "Name";
                }                    
            }
                
            validateButtons();
        }

        private void lbDomains_MouseDown(object sender, MouseEventArgs e)
        {
            if (lbDomains.SelectedIndex != -1)
            {
                validateButtons();
                lbDomains.DoDragDrop(lbDomains.SelectedIndex, DragDropEffects.Move);
            }
        }

        private void lbDomains_DragDrop(object sender, DragEventArgs e)
        {
            int from = (int)e.Data.GetData(typeof(int));
            if (from == -1)
                return;
            Point point = lbDomains.PointToClient(new Point(e.X, e.Y));
            int to = lbDomains.IndexFromPoint(point);
            if (to == -1) to = shell.Domains.Count - 1;
            if (from != to)
            {
                Functions.Swap(shell.Domains, from, to);
                lbDomains.SelectedIndex = to;
            }
        }

        private void lbDomains_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }
    }
}
