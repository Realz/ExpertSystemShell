﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemManager
{
    public enum VariableType { Request, Derivable, ReqDerivable, Const}
    [Serializable]
    public class Variable
    {
        public string ToList
        {
            get { return String.Format("{0} = {1}", Name, Value.Name); }
        }
        public string Name { get; set; }
        public string DomainName { get { return Domain.Name; } }
        public VariableType VarType { get; set; }
        public string TypeVarRus { get
            {
                string result;
                switch (VarType)
                {
                    case VariableType.Request:
                        result = "Запрашиваемая";
                        break;
                    case VariableType.Derivable:
                        result = "Выводимая";
                        break;
                    case VariableType.ReqDerivable:
                        result = "Выводимо-запрашиваемая";
                        break;
                    case VariableType.Const:
                        result = "Константа";
                        break;
                    default: result = "ERROR";
                        break;
                }
                return result;
            } }
        public Domain Domain { get; set; }
        public string Question { get; set; }
        public Value Value { get; set; }
        public bool isUsed { get { return usedByConclusion.Count > 0 || usedByPermises.Count > 0; } }
        public bool IsBroken;
        public HashSet<Rule> usedByConclusion;
        public HashSet<Rule> usedByPermises;

        public Variable()
        {
            IsBroken = false;
            usedByConclusion = new HashSet<Rule>();
            usedByPermises = new HashSet<Rule>();
        }
        public Variable(Variable v)
        {
            IsBroken = false;
            usedByConclusion = new HashSet<Rule>(v.usedByConclusion);
            usedByPermises = new HashSet<Rule>(v.usedByPermises);
            Domain = v.Domain;
            Question = v.Question;
            Value = v.Value;
            VarType = v.VarType;
            Name = v.Name;
        }
    }
}
