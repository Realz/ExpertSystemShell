﻿namespace ExpertSystemManager
{
    partial class AddEditDomainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btCancel = new System.Windows.Forms.Button();
            this.btOK = new System.Windows.Forms.Button();
            this.gbDomainName = new System.Windows.Forms.GroupBox();
            this.tbDomainName = new System.Windows.Forms.TextBox();
            this.gbDomainValues = new System.Windows.Forms.GroupBox();
            this.lbValues = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btDel = new System.Windows.Forms.Button();
            this.btChange = new System.Windows.Forms.Button();
            this.btAdd = new System.Windows.Forms.Button();
            this.tbValueInput = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbDomainName.SuspendLayout();
            this.gbDomainValues.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btCancel, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.btOK, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.gbDomainName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gbDomainValues, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(319, 450);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btCancel
            // 
            this.btCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCancel.Location = new System.Drawing.Point(162, 420);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(154, 27);
            this.btCancel.TabIndex = 7;
            this.btCancel.Text = "Отмена";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // btOK
            // 
            this.btOK.BackColor = System.Drawing.Color.YellowGreen;
            this.btOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btOK.Location = new System.Drawing.Point(3, 420);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(153, 27);
            this.btOK.TabIndex = 6;
            this.btOK.Text = "btOK";
            this.btOK.UseVisualStyleBackColor = false;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // gbDomainName
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gbDomainName, 2);
            this.gbDomainName.Controls.Add(this.tbDomainName);
            this.gbDomainName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbDomainName.Location = new System.Drawing.Point(3, 3);
            this.gbDomainName.Name = "gbDomainName";
            this.gbDomainName.Size = new System.Drawing.Size(313, 44);
            this.gbDomainName.TabIndex = 0;
            this.gbDomainName.TabStop = false;
            this.gbDomainName.Text = "Имя домена";
            // 
            // tbDomainName
            // 
            this.tbDomainName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbDomainName.Location = new System.Drawing.Point(3, 16);
            this.tbDomainName.Name = "tbDomainName";
            this.tbDomainName.Size = new System.Drawing.Size(307, 20);
            this.tbDomainName.TabIndex = 0;
            this.tbDomainName.TextChanged += new System.EventHandler(this.tbDomainName_TextChanged);
            // 
            // gbDomainValues
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gbDomainValues, 2);
            this.gbDomainValues.Controls.Add(this.lbValues);
            this.gbDomainValues.Controls.Add(this.groupBox3);
            this.gbDomainValues.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbDomainValues.Location = new System.Drawing.Point(3, 53);
            this.gbDomainValues.Name = "gbDomainValues";
            this.gbDomainValues.Size = new System.Drawing.Size(313, 361);
            this.gbDomainValues.TabIndex = 1;
            this.gbDomainValues.TabStop = false;
            this.gbDomainValues.Text = "Значения домена";
            // 
            // lbValues
            // 
            this.lbValues.AllowDrop = true;
            this.lbValues.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbValues.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbValues.FormattingEnabled = true;
            this.lbValues.ItemHeight = 20;
            this.lbValues.Location = new System.Drawing.Point(3, 16);
            this.lbValues.Name = "lbValues";
            this.lbValues.Size = new System.Drawing.Size(307, 200);
            this.lbValues.TabIndex = 1;
            this.lbValues.TabStop = false;
            this.lbValues.SelectedIndexChanged += new System.EventHandler(this.lbValues_SelectedIndexChanged);
            this.lbValues.DragDrop += new System.Windows.Forms.DragEventHandler(this.lbValues_DragDrop);
            this.lbValues.DragOver += new System.Windows.Forms.DragEventHandler(this.lbValues_DragOver);
            this.lbValues.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbValues_MouseDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel2);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(3, 216);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(307, 142);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Значение";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.btDel, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.btChange, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btAdd, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbValueInput, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(301, 123);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // btDel
            // 
            this.btDel.BackColor = System.Drawing.Color.Tomato;
            this.btDel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btDel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btDel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btDel.Location = new System.Drawing.Point(3, 92);
            this.btDel.Name = "btDel";
            this.btDel.Size = new System.Drawing.Size(295, 28);
            this.btDel.TabIndex = 3;
            this.btDel.Text = "Удалить";
            this.btDel.UseVisualStyleBackColor = false;
            this.btDel.Click += new System.EventHandler(this.btDel_Click);
            // 
            // btChange
            // 
            this.btChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btChange.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btChange.Location = new System.Drawing.Point(3, 60);
            this.btChange.Name = "btChange";
            this.btChange.Size = new System.Drawing.Size(295, 26);
            this.btChange.TabIndex = 2;
            this.btChange.Text = "Редактировать";
            this.btChange.UseVisualStyleBackColor = true;
            this.btChange.Click += new System.EventHandler(this.btChange_Click);
            // 
            // btAdd
            // 
            this.btAdd.BackColor = System.Drawing.Color.YellowGreen;
            this.btAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btAdd.Location = new System.Drawing.Point(3, 28);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(295, 26);
            this.btAdd.TabIndex = 1;
            this.btAdd.Text = "Добавить";
            this.btAdd.UseVisualStyleBackColor = false;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // tbValueInput
            // 
            this.tbValueInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbValueInput.Location = new System.Drawing.Point(3, 3);
            this.tbValueInput.Name = "tbValueInput";
            this.tbValueInput.Size = new System.Drawing.Size(295, 20);
            this.tbValueInput.TabIndex = 0;
            this.tbValueInput.TextChanged += new System.EventHandler(this.tbValueInput_TextChanged);
            // 
            // AddEditDomainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddEditDomainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddEditDomainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddEditDomainForm_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbDomainName.ResumeLayout(false);
            this.gbDomainName.PerformLayout();
            this.gbDomainValues.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gbDomainName;
        private System.Windows.Forms.GroupBox gbDomainValues;
        private System.Windows.Forms.ListBox lbValues;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox tbValueInput;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.Button btChange;
        private System.Windows.Forms.Button btDel;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.TextBox tbDomainName;
    }
}