﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemManager
{
    public partial class VariableForm : Form
    {
        private int i { get { return dgvVariables.SelectedCells.Count > 0 ? dgvVariables.SelectedCells[0].RowIndex : -1; } }
        Shell shell;
        bool isDragDrop;
        int DragDropIndex;
        public VariableForm()
        {
            InitializeComponent();
            dgvVariables.AutoGenerateColumns = false;
            shell = Shell.Instance;
            lbDomainValues.DisplayMember = "Name";
            dgvVariables.DataSource = shell.Variables;
            isDragDrop = false;
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            var addForm = new AddEditVariableForm(FormType.Add, new Variable());
            if (addForm.ShowDialog() == DialogResult.OK)
            {
                if (i == -1)
                {
                    shell.Variables.Add(addForm.GetVariable());
                    dgvVariables.Rows[shell.Variables.Count - 1].Selected = true;
                }
                else
                {
                    shell.Variables.Insert(i + 1, addForm.GetVariable());
                    dgvVariables.Rows[i + 1].Selected = true;
                }
                dgvVariables.FirstDisplayedScrollingRowIndex = i;
                addForm.GetVariable().Domain.usedBy.Add(addForm.GetVariable());
                
                shell.VariablesCount++;
            }
            validateButtons();
        }

        private void btChange_Click(object sender, EventArgs e)
        {
            Variable oldVariable = shell.Variables[i];
            AddEditVariableForm editForm;
            if (oldVariable.isUsed)
                editForm = new AddEditVariableForm(FormType.Edit, new Variable(oldVariable), true);
            else
                editForm = new AddEditVariableForm(FormType.Edit, new Variable(oldVariable));

            if (editForm.ShowDialog() == DialogResult.OK)
            {
                if (!oldVariable.isUsed)
                {
                    oldVariable.Domain.usedBy.Remove(oldVariable);
                    editForm.GetVariable().Domain.usedBy.Add(editForm.GetVariable());
                }
                CopyValues(editForm.GetVariable(), oldVariable);
                tbQuestion.Text = oldVariable.Question;
            }
            dgvVariables.Refresh();
        }

        private void validateButtons()
        {
            btDel.Enabled = btChange.Enabled = i != -1;
            if (i != -1)
            {
                lbDomainValues.DataSource = shell.Variables[i].Domain.Values;
                lbDomainValues.ClearSelected();
                if (shell.Variables[i].VarType == VariableType.Derivable)
                    tbQuestion.Clear();
                else
                    tbQuestion.Text = shell.Variables[i].Question;
                tbVariableType.Text = shell.Variables[i].TypeVarRus;
            }
        }

        private void CopyValues(Variable from, Variable to)
        {
            to.Domain = from.Domain;
            to.Question = from.Question;
            to.Value = from.Value;
            to.VarType = from.VarType;
            to.Name = from.Name;
        }

        private void btDel_Click(object sender, EventArgs e)
        {
            if (shell.Variables[i].isUsed)
                MessageBox.Show("Переменная используется");
            else
            {
                shell.Variables[i].Domain.usedBy.Remove(shell.Variables[i]);
                shell.Variables.RemoveAt(i);

                if (i == -1)
                {
                    lbDomainValues.DataSource = null;
                    lbDomainValues.DisplayMember = "Name";
                }
            }

            validateButtons();
        }

        private void dgvVariables_SelectionChanged(object sender, EventArgs e)
        {
            if (isDragDrop)
            {
                isDragDrop = false;
                dgvVariables.Rows[DragDropIndex].Selected = true;                
            }
            else
                validateButtons();
        }

        private void dgvVariables_DragDrop(object sender, DragEventArgs e)
        {
            int from = (int)e.Data.GetData(typeof(int));
            if (from == -1)
                return;
            Point point = dgvVariables.PointToClient(new Point(e.X, e.Y));
            int to = dgvVariables.HitTest(point.X, point.Y).RowIndex;
            if (to == -1) to = shell.Variables.Count - 1;
            if (from != to)
            {
                Functions.Swap(shell.Variables, from, to);
                dgvVariables.Rows[to].Selected = true;
                DragDropIndex = to;
                isDragDrop = true;
            }
            
        }

        private void dgvVariables_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void dgvVariables_MouseDown(object sender, MouseEventArgs e)
        {
            int index = dgvVariables.HitTest(e.X, e.Y).RowIndex;
            if (index != -1)
            {                
                dgvVariables.Rows[index].Selected = true;
                dgvVariables.DoDragDrop(i, DragDropEffects.Move);                
            }
        }
    }
}
