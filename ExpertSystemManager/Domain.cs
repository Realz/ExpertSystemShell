﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemManager
{
    [Serializable]
    public class Domain
    {
        public string Name { get; set; }
        public BindingList<Value> Values { get; set; }
        public bool isUsed { get { return usedBy.Count > 0; } }
        public HashSet<Variable> usedBy;

        public Domain()
        {
            Values = new BindingList<Value>();
            usedBy = new HashSet<Variable>();
        }

        public Domain(Domain d)
        {
            Name = d.Name;
            Values = new BindingList<Value>(d.Values);
            usedBy = new HashSet<Variable>(d.usedBy);
        }
    }
}
