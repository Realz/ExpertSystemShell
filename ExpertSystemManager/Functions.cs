﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemManager
{
    public static class Functions
    {
        public static void ChangeForm(Form f, Button b, FormType t, string what)
        {
            switch (t)
            {
                case FormType.Add:
                    f.Text = "Добавление " + what;
                    b.Text = "Добавить";
                    break;
                case FormType.Edit:
                    f.Text = "Изменение " + what;
                    b.Text = "Применить";
                    break;
                default:
                    break;
            }
        }

        public static void CheckButtons<T>(Button btAdd, Button btChange, Button btDel, BindingList<T> vals, string text, int selectedIndex) where T : IMyEqual
        {
            if (String.IsNullOrWhiteSpace(text))
            {
                btAdd.Enabled = btChange.Enabled = btDel.Enabled = false;
                return;
            }

            bool isNewName = true;
            for (int i = 0; i < vals.Count && isNewName; i++)
                if (vals[i].Equal(text))
                    isNewName = false;
            btAdd.Enabled = btChange.Enabled = isNewName;

            if (selectedIndex == -1)
                btDel.Enabled = btChange.Enabled = false;
            else
                btDel.Enabled = true;
        }

        public static void Swap<T>(BindingList<T> list, int from, int to) 
        {
            T tmp = list[from];
            if (to >= list.Count)
                to = list.Count - 1;
            list[from] = list[to];
            list[to] = tmp;
        }
    }
}
