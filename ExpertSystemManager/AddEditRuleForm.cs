﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemManager
{
    public partial class AddEditRuleForm : Form
    {
        int pi { get { return lbPermises.SelectedIndex; } }
        int ci { get { return lbConclusions.SelectedIndex; } }
        int tempIndex;
        Shell shell;
        Rule currentRule;
        bool isPermisesDrop;
        HashSet<string> rulesNames;
        bool validName, validPermisesCount, validConclusionCount, hasChanged, btOkPressed = false;

        public AddEditRuleForm(FormType tpe, Rule rule)
        {
            InitializeComponent();
            Functions.ChangeForm(this, btOK, tpe, "правила");
            shell = Shell.Instance;
            rulesNames = new HashSet<string>(shell.Rules.Select(i => i.Name).ToList());
            currentRule = rule;
            lbPermises.DataSource = currentRule.Premises;
            lbConclusions.DataSource = currentRule.Conclusions;
            lbPermises.DisplayMember = lbConclusions.DisplayMember = "ToList";

            if (tpe == FormType.Add)
            {
                tbName.Text = "Правило" + (shell.RulesCount + 1).ToString();
            }
            else
            {
                rulesNames.Remove(currentRule.Name);
                tbName.Text = currentRule.Name;
                rtbReason.Text = currentRule.Reason;
            }
            hasChanged = false;

            validatePermises();
            validateConclusion();
            validateRuleName();
            validateButtons(btEditPermises, btDelPermises, pi);
            validateButtons(btEditConclusion, btDelConclusion, ci);
        }

        public Rule GetRule()
        {
            return currentRule;
        }

        private void btAddPermises_Click(object sender, EventArgs e)
        {
            AddEditFactForm addPermises = new AddEditFactForm(FormType.Add, FactType.Permises, new Fact());
            if (addPermises.ShowDialog() == DialogResult.OK)
            {
                currentRule.Premises.Add(addPermises.GetFact());
                lbPermises.SelectedIndex = currentRule.Premises.Count - 1;
                validateButtons(btEditPermises, btDelPermises, pi);
                //currentRule.Premises.Last().var.usedBy.Add(currentRule);
                hasChanged = true;
                validatePermises();
                validatebtOK();
            }
        }

        private void btAddConclusion_Click(object sender, EventArgs e)
        {
            AddEditFactForm addConclusion = new AddEditFactForm(FormType.Add, FactType.Conclusion, new Fact());
            if (addConclusion.ShowDialog() == DialogResult.OK)
            {
                currentRule.Conclusions.Add(addConclusion.GetFact());
                lbConclusions.SelectedIndex = currentRule.Conclusions.Count - 1;
                validateButtons(btEditConclusion, btDelConclusion, ci);
                //currentRule.Conclusions.Last().var.usedBy.Add(currentRule);
                hasChanged = true;
                validateConclusion();
                validatebtOK();
            }
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            btOkPressed = true;
        }

        private void AddEditRuleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (hasChanged && !btOkPressed)
            {
                switch (MessageBox.Show("Сохранить изменения?", "Отмена", MessageBoxButtons.YesNoCancel))
                {
                    case DialogResult.Yes:
                        if (btOK.Enabled)
                            DialogResult = DialogResult.OK;
                        else
                            e.Cancel = true;
                        break;
                    case DialogResult.No: return;
                    default:
                        e.Cancel = true;
                        break;
                }
            }
            btOkPressed = false;
        }

        private void lbPermises_SelectedIndexChanged(object sender, EventArgs e)
        {
            validateButtons(btEditPermises, btDelPermises, pi);
        }

        private void lbConclusions_SelectedIndexChanged(object sender, EventArgs e)
        {
            validateButtons(btEditConclusion, btDelConclusion, ci);
        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {
            validateRuleName();
        }

        private void btEditPermises_Click(object sender, EventArgs e)
        {
            AddEditFactForm editPermises = new AddEditFactForm(FormType.Edit, FactType.Permises, new Fact(currentRule.Premises[pi]));
            if (editPermises.ShowDialog() == DialogResult.OK)
            {
                currentRule.Premises[pi].var = editPermises.GetFact().var;
                currentRule.Premises[pi].value = editPermises.GetFact().value;
                tempIndex = pi;
                lbPermises.SelectedIndex = -1;
                lbPermises.SelectedIndex = tempIndex;
                validatePermises();
                validatebtOK();
            }
        }

        private void btDelPermises_Click(object sender, EventArgs e)
        {
            //currentRule.Premises[pi].var.usedByPermises.Remove(currentRule);
            currentRule.Premises.RemoveAt(pi);
            validatePermises();
            validatebtOK();
        }

        private void btDelConclusion_Click(object sender, EventArgs e)
        {
            //currentRule.Conclusions[ci].var.usedBy.Remove(currentRule);
            currentRule.Conclusions.RemoveAt(ci);
            validateConclusion();
            validatebtOK();
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btEditConclusion_Click(object sender, EventArgs e)
        {
            AddEditFactForm editConclusion = new AddEditFactForm(FormType.Edit, FactType.Conclusion, new Fact(currentRule.Conclusions[ci]));
            if (editConclusion.ShowDialog() == DialogResult.OK)
            {
                currentRule.Conclusions[ci].var = editConclusion.GetFact().var;
                currentRule.Conclusions[ci].value = editConclusion.GetFact().value;
                tempIndex = ci;
                lbConclusions.SelectedIndex = -1;
                lbConclusions.SelectedIndex = tempIndex;
                validateConclusion();
                validatebtOK();
            }
        }

        private void validateButtons(Button btChange, Button btDel, int index)
        {
            btDel.Enabled = btChange.Enabled = index != -1;
        }

        private void rtbReason_TextChanged(object sender, EventArgs e)
        {
            currentRule.Reason = rtbReason.Text;
            hasChanged = true;
        }

        private void lbPermises_MouseDown(object sender, MouseEventArgs e)
        {
            if (pi != -1)
            {
                validatePermises();
                isPermisesDrop = true;
                lbPermises.DoDragDrop(pi, DragDropEffects.Move);
                
            }
        }

        private void lbPermises_DragOver(object sender, DragEventArgs e)
        {
            if (isPermisesDrop)
                e.Effect = DragDropEffects.Move;
        }

        private void lbPermises_DragDrop(object sender, DragEventArgs e)
        {
            int from = (int)e.Data.GetData(typeof(int));
            if (from == -1)
                return;
            Point point = lbPermises.PointToClient(new Point(e.X, e.Y));
            int to = lbPermises.IndexFromPoint(point);
            if (to == -1) to = currentRule.Premises.Count - 1;
            if (from != to)
            {
                Functions.Swap(currentRule.Premises, from, to);
                lbPermises.SelectedIndex = to;
            }
        }

        private void lbConclusions_MouseDown(object sender, MouseEventArgs e)
        {
            if (ci != -1)
            {
                validatePermises();
                isPermisesDrop = false;
                lbConclusions.DoDragDrop(ci, DragDropEffects.Move);                
            }
        }

        private void lbConclusions_DragDrop(object sender, DragEventArgs e)
        {
            int from = (int)e.Data.GetData(typeof(int));
            if (from == -1)
                return;
            Point point = lbConclusions.PointToClient(new Point(e.X, e.Y));
            int to = lbConclusions.IndexFromPoint(point);
            if (to == -1) to = currentRule.Conclusions.Count - 1;
            if (from != to)
            {
                Functions.Swap(currentRule.Conclusions, from, to);
                lbConclusions.SelectedIndex = to;
            }
        }

        private void lbConclusions_DragOver(object sender, DragEventArgs e)
        {
            if (!isPermisesDrop)
                e.Effect = DragDropEffects.Move;
        }

        private void validateRuleName()
        {
            if (!String.IsNullOrWhiteSpace(tbName.Text) && !rulesNames.Contains(tbName.Text))
            {
                gbName.ForeColor = DefaultForeColor;
                gbName.Text = "Имя правила";
                validName = true;
                currentRule.Name = tbName.Text;
            }
            else
            {
                gbName.ForeColor = Color.Red;
                gbName.Text = "Имя правила (Недопустимое)";
                validName = false;
            }
        }

        private void validatePermises()
        {
            if (currentRule.Premises.Count > 0)
            {
                gbPremisess.ForeColor = DefaultForeColor;
                gbPremisess.Text = "Условия правила";
                validPermisesCount = true;
            }
            else
            {
                gbPremisess.ForeColor = Color.Red;
                gbPremisess.Text = "Условия правила (Отсутствуют)";
                validPermisesCount = false;
            }
        }

        private void validateConclusion()
        {
           if (currentRule.Conclusions.Count > 0)
            {
                gbConclusions.ForeColor = DefaultForeColor;
                gbConclusions.Text = "Заключения правила";
                validConclusionCount = true;
            }
            else
            {
                gbConclusions.ForeColor = Color.Red;
                gbConclusions.Text = "Заключения правила (Отсутствуют)";
                validConclusionCount = false;
            }
        }

        private void validatebtOK()
        {
            btOK.Enabled = validName && validPermisesCount && validConclusionCount;
        }
    }
}
