﻿namespace ExpertSystemManager
{
    partial class AddEditVariableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btCancel = new System.Windows.Forms.Button();
            this.btOK = new System.Windows.Forms.Button();
            this.gbVariableName = new System.Windows.Forms.GroupBox();
            this.tbVariableName = new System.Windows.Forms.TextBox();
            this.gbDomain = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.gbDomainValues = new System.Windows.Forms.GroupBox();
            this.lbDomainValues = new System.Windows.Forms.ListBox();
            this.cbDomain = new System.Windows.Forms.ComboBox();
            this.btAddDomain = new System.Windows.Forms.Button();
            this.gbVariableType = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.rbVivod = new System.Windows.Forms.RadioButton();
            this.rbVivodZapros = new System.Windows.Forms.RadioButton();
            this.tbQuestion = new System.Windows.Forms.TextBox();
            this.rbZapros = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbVariableName.SuspendLayout();
            this.gbDomain.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.gbDomainValues.SuspendLayout();
            this.gbVariableType.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btCancel, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.btOK, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.gbVariableName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gbDomain, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.gbVariableType, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(309, 431);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btCancel
            // 
            this.btCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCancel.Location = new System.Drawing.Point(157, 398);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(149, 30);
            this.btCancel.TabIndex = 7;
            this.btCancel.Text = "Отмена";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // btOK
            // 
            this.btOK.BackColor = System.Drawing.Color.YellowGreen;
            this.btOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btOK.Location = new System.Drawing.Point(3, 398);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(148, 30);
            this.btOK.TabIndex = 6;
            this.btOK.Text = "btOK";
            this.btOK.UseVisualStyleBackColor = false;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // gbVariableName
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gbVariableName, 2);
            this.gbVariableName.Controls.Add(this.tbVariableName);
            this.gbVariableName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbVariableName.Location = new System.Drawing.Point(3, 3);
            this.gbVariableName.Name = "gbVariableName";
            this.gbVariableName.Size = new System.Drawing.Size(303, 39);
            this.gbVariableName.TabIndex = 0;
            this.gbVariableName.TabStop = false;
            this.gbVariableName.Text = "Имя";
            // 
            // tbVariableName
            // 
            this.tbVariableName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbVariableName.Location = new System.Drawing.Point(3, 16);
            this.tbVariableName.Name = "tbVariableName";
            this.tbVariableName.Size = new System.Drawing.Size(297, 20);
            this.tbVariableName.TabIndex = 0;
            this.tbVariableName.TextChanged += new System.EventHandler(this.tbVariableName_TextChanged);
            // 
            // gbDomain
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gbDomain, 2);
            this.gbDomain.Controls.Add(this.tableLayoutPanel2);
            this.gbDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbDomain.Location = new System.Drawing.Point(3, 48);
            this.gbDomain.Name = "gbDomain";
            this.gbDomain.Size = new System.Drawing.Size(303, 169);
            this.gbDomain.TabIndex = 1;
            this.gbDomain.TabStop = false;
            this.gbDomain.Text = "Домен";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.Controls.Add(this.gbDomainValues, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.cbDomain, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btAddDomain, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(297, 150);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // gbDomainValues
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.gbDomainValues, 2);
            this.gbDomainValues.Controls.Add(this.lbDomainValues);
            this.gbDomainValues.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbDomainValues.Location = new System.Drawing.Point(3, 30);
            this.gbDomainValues.Name = "gbDomainValues";
            this.gbDomainValues.Size = new System.Drawing.Size(291, 117);
            this.gbDomainValues.TabIndex = 1;
            this.gbDomainValues.TabStop = false;
            this.gbDomainValues.Text = "Значения домена";
            // 
            // lbDomainValues
            // 
            this.lbDomainValues.AllowDrop = true;
            this.lbDomainValues.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDomainValues.FormattingEnabled = true;
            this.lbDomainValues.Location = new System.Drawing.Point(3, 16);
            this.lbDomainValues.Name = "lbDomainValues";
            this.lbDomainValues.Size = new System.Drawing.Size(285, 98);
            this.lbDomainValues.TabIndex = 0;
            this.lbDomainValues.TabStop = false;
            this.lbDomainValues.DragDrop += new System.Windows.Forms.DragEventHandler(this.lbDomainValues_DragDrop);
            this.lbDomainValues.DragOver += new System.Windows.Forms.DragEventHandler(this.lbDomainValues_DragOver);
            this.lbDomainValues.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbDomainValues_MouseDown);
            // 
            // cbDomain
            // 
            this.cbDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbDomain.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDomain.FormattingEnabled = true;
            this.cbDomain.Location = new System.Drawing.Point(3, 3);
            this.cbDomain.Name = "cbDomain";
            this.cbDomain.Size = new System.Drawing.Size(264, 21);
            this.cbDomain.TabIndex = 0;
            this.cbDomain.SelectedIndexChanged += new System.EventHandler(this.cbDomain_SelectedIndexChanged);
            // 
            // btAddDomain
            // 
            this.btAddDomain.BackColor = System.Drawing.Color.YellowGreen;
            this.btAddDomain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAddDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btAddDomain.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btAddDomain.Location = new System.Drawing.Point(273, 3);
            this.btAddDomain.Name = "btAddDomain";
            this.btAddDomain.Size = new System.Drawing.Size(21, 21);
            this.btAddDomain.TabIndex = 2;
            this.btAddDomain.Text = "+";
            this.btAddDomain.UseVisualStyleBackColor = false;
            this.btAddDomain.Click += new System.EventHandler(this.btAddDomain_Click);
            // 
            // gbVariableType
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gbVariableType, 2);
            this.gbVariableType.Controls.Add(this.tableLayoutPanel3);
            this.gbVariableType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbVariableType.Location = new System.Drawing.Point(3, 223);
            this.gbVariableType.Name = "gbVariableType";
            this.gbVariableType.Size = new System.Drawing.Size(303, 169);
            this.gbVariableType.TabIndex = 2;
            this.gbVariableType.TabStop = false;
            this.gbVariableType.Text = "Тип";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.rbVivod, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.rbVivodZapros, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.tbQuestion, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.rbZapros, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(297, 150);
            this.tableLayoutPanel3.TabIndex = 4;
            // 
            // rbVivod
            // 
            this.rbVivod.AutoSize = true;
            this.rbVivod.Checked = true;
            this.rbVivod.Location = new System.Drawing.Point(3, 3);
            this.rbVivod.Name = "rbVivod";
            this.rbVivod.Size = new System.Drawing.Size(84, 17);
            this.rbVivod.TabIndex = 0;
            this.rbVivod.TabStop = true;
            this.rbVivod.Text = "Выводимая";
            this.rbVivod.UseVisualStyleBackColor = true;
            this.rbVivod.Click += new System.EventHandler(this.rbVivod_Click);
            // 
            // rbVivodZapros
            // 
            this.rbVivodZapros.AutoSize = true;
            this.rbVivodZapros.Location = new System.Drawing.Point(3, 53);
            this.rbVivodZapros.Name = "rbVivodZapros";
            this.rbVivodZapros.Size = new System.Drawing.Size(163, 17);
            this.rbVivodZapros.TabIndex = 2;
            this.rbVivodZapros.Text = "Выводимо-запрашиваемая";
            this.rbVivodZapros.UseVisualStyleBackColor = true;
            this.rbVivodZapros.CheckedChanged += new System.EventHandler(this.rbVivodZapros_CheckedChanged);
            // 
            // tbQuestion
            // 
            this.tbQuestion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbQuestion.Location = new System.Drawing.Point(3, 78);
            this.tbQuestion.Multiline = true;
            this.tbQuestion.Name = "tbQuestion";
            this.tbQuestion.Size = new System.Drawing.Size(291, 69);
            this.tbQuestion.TabIndex = 3;
            // 
            // rbZapros
            // 
            this.rbZapros.AutoSize = true;
            this.rbZapros.Location = new System.Drawing.Point(3, 28);
            this.rbZapros.Name = "rbZapros";
            this.rbZapros.Size = new System.Drawing.Size(108, 17);
            this.rbZapros.TabIndex = 1;
            this.rbZapros.Text = "Запрашиваемая";
            this.rbZapros.UseVisualStyleBackColor = true;
            this.rbZapros.Click += new System.EventHandler(this.rbZapros_Click);
            // 
            // AddEditVariableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 431);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddEditVariableForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddEditVariableForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddEditVariableForm_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbVariableName.ResumeLayout(false);
            this.gbVariableName.PerformLayout();
            this.gbDomain.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.gbDomainValues.ResumeLayout(false);
            this.gbVariableType.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gbVariableName;
        private System.Windows.Forms.TextBox tbVariableName;
        private System.Windows.Forms.GroupBox gbDomain;
        private System.Windows.Forms.GroupBox gbVariableType;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox gbDomainValues;
        private System.Windows.Forms.ComboBox cbDomain;
        private System.Windows.Forms.ListBox lbDomainValues;
        private System.Windows.Forms.Button btAddDomain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.RadioButton rbVivod;
        private System.Windows.Forms.RadioButton rbVivodZapros;
        private System.Windows.Forms.TextBox tbQuestion;
        private System.Windows.Forms.RadioButton rbZapros;
    }
}